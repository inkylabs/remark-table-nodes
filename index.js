import visit from 'unist-util-visit'

// TODO: Write tests
export default (opts) => {
  opts = Object.assign({
  }, opts)

  return (root, f) => {
    visit(root, 'table', (node, index, parent) => {
      // Set alignment.
      visit(node, 'tableCell', (n, i) => {
        n.align = node.align[i]
      })

      // remark-gfm is not correctly reading the header, so let's fake it for
      // now. https://github.com/remarkjs/remark-gfm/issues/5
      const hnode = node.children[0]
      hnode.header = true
      visit(hnode, 'tableRow', n => { n.header = true })
      visit(hnode, 'tableCell', n => { n.header = true })

      const bodyChildren = node.children.slice(1, node.children.length)

      node.children = [
        {
          type: 'tableHead',
          position: hnode.position,
          children: [hnode]
        }
      ]
      if (!bodyChildren.length) return
      node.children.push({
        type: 'tableBody',
        position: {
          start: bodyChildren[0].position.start,
          end: bodyChildren[bodyChildren.length - 1].position.end
        },
        children: bodyChildren
      })
    })
  }
}
